#!/bin/bash

PWLEN=25
BASEDIR=$(dirname "$0")

# Generates a random password
function generatePassword() {
	pass generate -fc gen $PWLEN
	echo $(xclip -selection c -o)
}

echo "Insert alias name:"
read ALIAS

echo "Insert account name:"
read ACC

echo "Generate password? (y/N):"
read GENERATE

if [ "$GENERATE" == "y" ] || [ "$GENERATE" == "Y" ]; then
	pass generate -fc tmp/pwgen $PWLEN
	PW1=$(xclip -selection c -o)
	PW2=$PW1
	pass rm -f tmp/pwgen
elif [ "$GENERATE" == "N" ] || [ "$GENERATE" == "n" ]; then
	echo "Insert password:"
	read -s PW1
	echo "Repeat password:"
	read -s PW2
else
	echo "Invalid answer. Expected (y/N)"
	exit 1
fi

if [ "$PW1" == "$PW2" ]; then
	printf %"s\n" $ACC $PW1 | pass insert -m $ALIAS

	if [ $? -eq 0 ]; then
		echo "Successfully added $ALIAS to password-manager"
	else
		echo "Error occured"
	fi

	exit $?
else
	echo "The passwords don't match"
	exit 2
fi

