#!/bin/bash

# into home
cd ~/

# flag if password-store already exists
pwse=$(ls -la | grep ".password-store" | wc -l)

# check if flag is true and ask user for deletion
if [ $pwse -gt 0 ]; then
	echo "password store already exists, do you wan't to override it? (y/n)"
	read del

	if [ "$del" == "y" ] || [ "$del" == "Y" ]; then
		rm -rf ./.password-store
	else
		echo "bye"
		exit 1
	fi
fi

echo "init empty password store..."
pass init ./

# ask for git (ssh) url to sync the password store with
echo "sync with git? (y/n)"
read sync

# sync: what this does is going into tmp, checkout the project, 
if [ "$sync" == "y" ] || [ "$sync" == "Y" ]; then
	echo ""
	echo "enter git repository url (ssh)"
	read url
	cd /tmp
	folder=$(echo "$url" | cut -d/ -f2 | sed -e "s/\.git//g")
	if [ -d $folder ]; then
		rm -rf $folder
	fi
	git clone $url
	cd $folder
	cp -rf ./ ~/.password-store/
	cd ..
	rm -rf $folder
fi

echo "bye"
