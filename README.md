# Description
pwmanager is a neat little password manager for dmenu based on pass.

## Prerequisites
Make sure you have the following tools installed since the setup script does not take care of those
1. dmenu
2. git

## Installation
To install the pwmanager you just have to run `./setup.sh`
If you don't have pacman as package manager you need to install the packages listed in `dependencies/pacman.txt` manually. Please get in touch with me if you need support for a different package manager.

## Getting Started
Execute `./init.sh` it will lead you through the process of creating a new pwstore or synchronizing with an existing one!

Once your password store is created you can add credentials with `./add.sh`.
Added credentials will be listed in dmenu after executing `pwmanager`

### Modes
You have two modes how pwmanager will output the decrypted password from your pwstore.
1. Regular: `pwmanager`
2. Password Only: `pwmanager --pw-only`

## Yubikey
You can store your GPG master key on a yubikey that will help you access your passwords. Support is not done yet.

# Todo
- Configurable
    - Colors
    - Modes
    - Behaviour
- Yubikey Support
