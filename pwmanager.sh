#!/bin/bash

# Lists all available aliases in dmenu and returns the selected one
function selectAliasFromDmenu() {
	DIR=~/.password-store/
	HINT="Credentials:"
	COLOR="#FFB86C"
	if [ "$1" == "true" ]; then
		HINT="Credentials (pw only):"
		COLOR="#FF79C6"
	fi

	find $DIR -name "*.gpg" | sed -e "s|\.gpg||g" | sed -e "s|$DIR||g" | dmenu -l 10 -p "$HINT" -nf '#F8F8F2' -nb '#282A36' -sb "$COLOR" -sf '#F8F8F2'
}

# Check if pw-only were given 
PW_ONLY=false
if [ "$1" == "--pw-only" ]; then
	PW_ONLY=true
fi

#1. Select alias from list of available credentials
SELECTED_ALIAS=$(selectAliasFromDmenu $PW_ONLY)

if [ "$SELECTED_ALIAS" != "" ]; then
  #2. Get credentials from password sotrage
  CREDENTIALS=$(pass $SELECTED_ALIAS)

  #3. Get account and password from credentials
  ACC=$(echo "$CREDENTIALS" | head -n 1)
  PW=$(echo "$CREDENTIALS" | tail -n 1)

  if [ "$PW_ONLY" == "false" ]; then
	  #4. Write out account name
	  xdotool type $ACC

	  #5. Tab to password
	  xdotool key Tab
  fi

  #6. Write out password
  xdotool type $PW

  #7. Press Enter
  xdotool key Return

  #8. Empty used vars
  ACC=""
  PW=""
fi
