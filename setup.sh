#!/bin/bash

#REL_PATH=$(dirname $0)
#ABS_PATH=$(readlink -f $0) 
ABS_DIR=$(dirname `realpath -s $0`)

# check if package is installed
function isInstalled() {
    command -v $1 > /dev/null 2>&1
}

isInstalled pwmanager
if [ $? -ne 0 ]; then
    isInstalled pacman
    if [ $? -ne 0 ]; then
        echo "only pacman is supported yet :("
        echo "you can manually install the following packages:"
        for line in $(cat $ABS_DIR/dependencies/pacman.txt)
        do
            echo "    $line"
        done
        exit 1
    fi

    echo "Checking dependencies:"
    for package in $(cat $ABS_DIR/dependencies/pacman.txt)
    do
        pacman -T $package
        if [ $? -eq 0 ]; then
            echo "    '$package' is already installed"
        else
            echo "    installing $package..."
            sudo pacman -S --needed $package
        fi
    done

    echo ""
    echo "installing pwmanager..."
    sudo ln -sf $ABS_DIR/pwmanager.sh /usr/bin/pwmanager

    if [ $? -eq 0 ]; then
        echo "all done, you can now init your password store using 'init.sh'"
        exit 0
    fi

    echo "ERROR: Couldn't create symlink"
    exit 2
fi

echo "pwmanager is already installed"
